﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAYORYMENOR

{
    class Program
    {
        private int[] vec
            ;

        public Program()
        {
            Console.Write("INGRESE EL TAMAÑO DEL VECTOR:");
            int T = int.Parse(Console.ReadLine());
            vec = new int[T];
        }

        public void Cargar()
        {
            for (var f = 0; f < vec.Length; f++)
            {
                Console.Write("ELEMENTOS:");
                vec[f] = int.Parse(Console.ReadLine());
            }
        }

        public void MayorMenor(out int MA, out int ME)
        {
            MA = vec[0];
            ME = vec[0];
            for (var f = 1; f < vec.Length ; f++)
            {
                if (vec[f] > MA)
                {
                    MA = vec[f];
                }
                else
                {
                    if (vec[f] < ME)
                    {
                        ME = vec[f];
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.Cargar();
            int ma, me;
            p.MayorMenor(out ma, out me);
            Console.WriteLine("EL MAYOR ES" + ma);
            Console.WriteLine("EL MENOR ES:" + me);
            Console.ReadKey();
        }
    }
}
